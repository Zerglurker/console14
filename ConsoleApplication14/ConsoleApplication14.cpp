﻿// ConsoleApplication14.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>
#include <time.h>

void PrintChet(int N, bool chet = 0) {

	for (int i = 0;i < N; i++)
		if ((i % 2 == 1) != chet)
			std::cout << i << "\n";

}


int main()
{
	const int N = 6;

	//lesson 14.3
	//std::string baseline{"test line"};
	//std::cout << "Input line:\n";
	//std::getline(std::cin, baseline);

	//std::cout << "string:" << baseline << "\n";
	//std::cout << "length:" << baseline.length() << "\n";
	//std::cout << "first:" << baseline[0] << " end:" << baseline[baseline.length() - 1];
	//std::cout << "first:" << baseline.front() << " end:" << baseline.back();

	//lesson 15.4
	//std::cout << "chet 1\n";
	//for (int i = 0; i < N; i++) {
	//	if (!(i % 2)) std::cout << i << "\n";
	//}

	//std::cout << "\nchet 2\n";
	//PrintChet(N, true);

	//lesson 16.5

	long aInt[N][N];
	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++)
			aInt[i][j] = i + j;

	using namespace std; //чтоб не мучаться с std::

	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++)
			cout << (j == 0 ? "" : ",") << aInt[i][j];
		cout << "\n";
	}

	time_t dt = time(NULL); //seconds since the Epoch  = _int64
	tm r_dt;                //struct{int tm_sec,int tm_min,int tm_hour, ...}
	localtime_s(&r_dt, &dt); //дадим ей указатели на наши клочки памяти с переменными

	//cout << "day:" << r_dt.tm_mday << "\n";

	int i = r_dt.tm_mday % N; // static_cast<int>(r_dt.tm_mday) ем?  там и так int

	//cout << "i:" << i;

	long sum = 0;
	for (int j = 0; j < N; j++)
		sum += aInt[i][j];

	cout << "sum[" << i << "] = " << sum << "\n";
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

